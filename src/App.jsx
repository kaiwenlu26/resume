import "./style.scss"; //debug
import { Mui_test } from "./TestComponents";
import styled from "styled-components";
import { Divider } from "./Components/Divider";
import { OtherLinks } from "./OtherLinks";
import { Introduction } from "./Introduction";
import { Skills } from "./Skills";

export default function App() {
  return (
    <div>
      <WrapDiv>
        <Divider text={"自我介紹"} />
        <Introduction />
        <Divider text={"熟悉度"} />
        <Skills />
        <Divider text={"學歷"} />
        <Mui_test data={taipeiTech} />
        <Mui_test data={daan} />
        <Divider text={"工作經歷"} />
        <Mui_test data={askey} />
        <Mui_test data={oj} />
        <Mui_test data={hsiang} />
        <Divider text={"個人專案"} />
        <Mui_test data={Mobile} />
        <Mui_test data={Resume} />
        <Mui_test data={Weather} />
        <Divider text={"其他資訊"} />
        <OtherLinks />
      </WrapDiv>
    </div>
  );
}

const Weather = {
  path: "img/weather.png",
  name: "一周氣溫及降雨機率",
  dept: "嘗試使用Vue開發做一個整合自己需要的天氣資訊網站",
  description: (
    <div>
      <a
        href="https://kluh174-weather.netlify.app/"
        target="_blank"
        rel="noreferrer"
      >
        網站連結
      </a>
      <ul>
        <li>
          使用Vue3開發搭配串接中央氣象資料開放平台中央氣象局開放資料平臺之資料擷取API
        </li>
        <li>
          將獲取資料透過Chart.js將台北一周最高、最低氣溫及降雨機率顯示在前端頁面
        </li>
      </ul>
    </div>
  )
};
const Resume = {
  path: "img/resume.png",
  name: "個人履歷網頁",
  dept: "也就是您目前所看到的網頁頁面",
  description: (
    <div>
      <a
        href="https://kluh174-resume.netlify.app"
        target="_blank"
        rel="noreferrer"
      >
        網站連結
      </a>
      <ul>
        <li>
          使用ReactJS及styled
          components來製作，一個擁有自己想要的排版及效果的個人履歷網站
        </li>
        <li>Vite打包並部屬在Netlify上呈現</li>
      </ul>
    </div>
  )
};

const Mobile = {
  path: "img/test.png",
  name: "Mobile端派工系統",
  dept: "手機端派工單系統",
  description: (
    <div>
      <a
        href="https://docs.google.com/document/d/1S8BrlmwIbPXlJ7NfJX9Fo1l-par8oXlz/edit?usp=drive_link&ouid=109057946376686211939&rtpof=true&sd=true"
        target="_blank"
        rel="noreferrer"
      >
        文件連結
      </a>
      <ul>
        <li>
          使用JavaScript ES6搭配PWA(漸進式網路應用程式)開發的手機端派工系統，
          可在Android及Apple雙平台中達到接近原生應用程式的效果
        </li>
        <li>
          遵循RESTful API 介面串接後端資料庫達成新增、修改、刪除、查詢等功能
        </li>
        <li>引用Html5-QRCode.js套件開啟手機鏡頭並上傳圖片至後端資料庫</li>
        <li>套用Bootstrap元件達成用户友好介面</li>
      </ul>
    </div>
  )
};
const taipeiTech = {
  path: "img/taipeiTech.jpg",
  name: "國立臺北科技大學",
  dept: "工業工程與管理系",
  period: "2018.09 ~ 2022.06"
};
const daan = {
  path: "img/TAIVS.png",
  name: "臺北市立大安高級工業職業學校",
  dept: "資訊科",
  period: "2015.09 ~ 2018.06"
};
const askey = {
  path: "img/askey.svg",
  name: "亞旭電腦股份有限公司",
  dept: "MES系統開發工程師",
  period: "2022.08 ~ 仍在職",

  description: (
    <div>
      <p>【工作內容】</p>
      <ul>
        <li>使用Delphi對WMS(倉庫管理系統)進行系統設計、功能開發及錯誤排除</li>
        <li>
          使用PL/SQL處理SFC(製程管理系統)及ERP(企業資源規劃)系統間資料流處理
        </li>
        <li>使用Java、C#對工廠端PDA、Webservice功能維護</li>
      </ul>
    </div>
  )
};
const oj = {
  path: "img/oj.jpg",
  name: "Oj Morning早午餐",
  dept: "實習員工",
  period: "2021.03 ~2021.06"
};
const hsiang = {
  path: "img/hsiang.jpg",
  name: "翔宇廣告",
  dept: "暑期作業員",
  period: "2016.06 ~ 2016.09"
};

const WrapDiv = styled.div`
  max-width: 700px;
  min-width: 480px;
  margin: 0 auto;
  padding: 2rem;
  text-align: center;
`;
