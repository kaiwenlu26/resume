import { LinearProgress } from "@mui/material";
import { PaperStyled } from "./Components/AllComponents";
import { StaticLinearProgressWithLabel } from "./Components/AllComponents";
import styled from "styled-components";
import { FlexRow } from "./Components/AllComponents";
import { Paper } from "@mui/material";
import { Fade } from "react-awesome-reveal";

const ExtendedPaper = styled(Paper)`
  && {
    display: flex;
    flex-direction: column;

    background-color: white;
    color: black;
    margin: 10px 0px 10px 0px;
    height: auto;
    width: 100%;
    padding: 10px;
    /* outline: black 1px solid; */
  }
`;
const Ptext = styled.div`
  width: 20%;
`;

const SkillComponent = (props) => {
  return (
    <FlexRow>
      <Ptext>{props.text}</Ptext>
      <StaticLinearProgressWithLabel number={props.per} />
    </FlexRow>
  );
};

export const Skills = () => {
  return (
    <Fade triggerOnce={true}>
      <ExtendedPaper>
        程式語言
        <SkillComponent text="Javascript" per={75} />
        <SkillComponent text="CSS" per={75} />
        <SkillComponent text="Delphi" per={65} />
        <SkillComponent text="C#" per={50} />
        <SkillComponent text="Java" per={30} />
        資料庫
        <SkillComponent text="Oracle" per={70} />
        <SkillComponent text="MS SQL" per={50} />
        框架
        <SkillComponent text="ReactJS" per={50} />
        <SkillComponent text="VueJS" per={40} />
        <SkillComponent text=".NET CORE" per={20} />
        其他工具
        <SkillComponent text="Git版控" per={75} />
        <SkillComponent text="npm" per={70} />
      </ExtendedPaper>
    </Fade>
  );
};
