import styled from "styled-components";
import GitHubCalendar from "react-github-calendar";
import { Fade } from "react-awesome-reveal";
import { PaperStyled } from "./Components/AllComponents";

const ExtendedPaperStyled = styled(PaperStyled)`
  padding: 10px;
  display: flex;
  flex-direction: column;
  align-items: start;
`;

export const OtherLinks = () => {
  return (
    <div>
      <Fade triggerOnce={true}>
        <ExtendedPaperStyled
          elevation={3}
          // onClick={() => window.open("https://github.com/Rys3t", "_blank")}
        >
          <GitHubCalendar username="Rys3t" colorScheme="light" />
          <p>
            <a href="https://github.com/Rys3t">Github</a>
          </p>
          Gmail : kluh174@gmail.com
        </ExtendedPaperStyled>
      </Fade>
    </div>
  );
};
