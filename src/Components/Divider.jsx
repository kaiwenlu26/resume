import styled from "styled-components";
import propTypes from "prop-types";

const StyledDiv = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
`;
const Title = styled.p`
  width: auto;
  min-width: 110px;
  white-space: nowrap;
  font-family: "Consolas";
  font-size: 1.25rem;
  display: block;
  color: white;
  background-color: #000080;
  border-radius: 3px;
`;

const StyledHr = styled.div`
  width: 100%;
  height: 1px;
  margin: auto;
  margin-left: 10px;
  border-top: solid #000080 3px;
`;

export const Divider = (props) => {
  return (
    <StyledDiv>
      <Title>{props.text}</Title>
      <StyledHr />
    </StyledDiv>
  );
};
Divider.propTypes = {
  text: propTypes.string.isRequired
};
