import styled from "styled-components";
import { Paper } from "@mui/material";
import LinearProgress from "@mui/material/LinearProgress";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import PropTypes from "prop-types";

export const PaperStyled = styled(Paper)`
  && {
    display: flex;
    background-color: white;
    color: black;
    margin: 10px 0px 10px 0px;
    height: auto;
    /* outline: black 1px solid; */
  }
`;
export const FlexColumn = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;
export const FlexRow = styled.div`
  display: flex;
  flex-direction: row;
`;
export const Picon = styled.p`
  font-size: 1rem;
  color: white;
  background-color: black;
`;

function LinearProgressWithLabel(props) {
  return (
    <Box sx={{ display: "flex", alignItems: "center" }}>
      <Box sx={{ width: "80%", mr: 1 }}>
        <LinearProgress variant="determinate" value={props.value} />
      </Box>
      <Box sx={{ minWidth: 35 }}>
        <Typography variant="body2" color="text.secondary">{`${Math.round(
          props.value
        )}%`}</Typography>
      </Box>
    </Box>
  );
}

export function StaticLinearProgressWithLabel(props) {
  return (
    <Box sx={{ width: "80%" }}>
      <LinearProgressWithLabel value={props.number} />
    </Box>
  );
}
