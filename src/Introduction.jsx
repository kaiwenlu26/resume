import { PaperStyled } from "./Components/AllComponents";
import styled from "styled-components";
import { Fade } from "react-awesome-reveal";

const ExtendedPaper = styled(PaperStyled)`
  padding: 10px;
  text-align: start;
`;
const ImgStyled = styled.img`
  height: auto;
  width: auto;
  max-height: 100px;
  margin-right: 10px;
`;

export const Introduction = () => {
  return (
    <Fade triggerOnce={true}>
      <ExtendedPaper>
        <ImgStyled src="img/me.png" />
        您好，我是陸凱文，目前為亞旭電腦的MES系統開發工程師，在程式方面有相當大的興趣和喜好，喜愛與他人合作共同完成工作上的難題，
        藉由每次的機會學習，不斷地往成為程式設計師的方向邁進。
      </ExtendedPaper>
    </Fade>
  );
};
