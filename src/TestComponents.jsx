// import React from "react";
import styled from "styled-components";
import propTypes from "prop-types";
import { Fade } from "react-awesome-reveal";
import { FlexColumn, FlexRow, PaperStyled } from "./Components/AllComponents";

const SquareImg = styled.img`
  width: auto;
  height: auto;
  max-width: 100px;
  /* max-height: 70px; */
  margin: 0px auto;
  /* vertical-align: middle; */
  /* outline: 1px gray solid; */
`;
const ImgContainer = styled.div`
  margin: 10px;
  min-height: 100px;
  width: 100px;
  display: flex;
  align-items: center;
`;
const TitleDiv = styled.div`
  display: flex;
  flex-direction: column;
  margin: 10px 0px 10px 10px;
  justify-content: center;
  align-items: start;
  /* margin: 0 auto; */
`;
const Text = styled.p`
  font-family: "Courier New", Courier, monospace;
  font-size: 1em;
  margin: 0px;
`;
const DeptText = styled(Text)`
  font-size: 0.6em;
  color: gray;
`;
const DescriptionDiv = styled(FlexColumn)`
  width: 100%;
  align-items: start; //靠左對齊
  justify-items: self-start;
  padding: 0px 10px 0px 10px;
  text-align: left;
`;
const StyledHr = styled.div`
  width: 100%;
  height: 1px;
  margin: auto;
  padding: 5px;
  border-top: solid gray 0.5px;
`;
export const Mui_test = (props) => {
  //const path = "/img/taipeiTech.jpg";

  return (
    <div>
      <Fade triggerOnce={true}>
        <PaperStyled elevation={3}>
          <FlexColumn>
            <FlexRow>
              <ImgContainer>
                <SquareImg src={props.data.path} alt="image" />
              </ImgContainer>
              <TitleDiv>
                <Text>{props.data.name}</Text>
                <DeptText>{props.data.dept}</DeptText>
                <DeptText>{props.data.period}</DeptText>
              </TitleDiv>
            </FlexRow>
            {props.data.description && (
              <DescriptionDiv>
                <StyledHr />
                {props.data.description}
              </DescriptionDiv>
            )}
          </FlexColumn>
        </PaperStyled>
      </Fade>
    </div>
  );
};
Mui_test.propTypes = {
  data: propTypes.object.isRequired
};
